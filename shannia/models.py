from django.db import models
from alghi.models import Pet

class AskForPet(models.Model):
    name = models.CharField(max_length = 50)
    phone_number = models.CharField(max_length = 50)
    e_mail = models.EmailField(max_length = 200)
    message = models.TextField(max_length = 500, default="some messages.")
    user = models.ForeignKey(Pet,on_delete=models.CASCADE, blank=True, null=True)
