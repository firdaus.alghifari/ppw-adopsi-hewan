from django.forms import ModelForm

from .models import AskForPet

class PetAsking(ModelForm):
    class Meta:
        model = AskForPet
        fields = [
            'name', 'phone_number', 'e_mail', 'message',
        ]
