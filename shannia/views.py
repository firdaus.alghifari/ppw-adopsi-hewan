from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import PetAsking
from .models import AskForPet
from alghi.models import Pet, PhotoPet
from alghi.forms import PetForm
from django.http import HttpResponseRedirect

def detail(request, detail_id):
    detailPet = Pet.objects.get(id=detail_id)
    photo_list = PhotoPet.objects.all()
    form = PetAsking()
    if request.method =="POST":
        form = PetAsking(request.POST)
        if form.is_valid():
            pet_form = AskForPet(name= form['name'].value(),phone_number= form['phone_number'].value(),
            e_mail= form['e_mail'].value(),message= form['message'].value(),user=detailPet)
            pet_form.save()
            return redirect('shannia:detail', detail_id=detail_id)
    else:
        AskForPets = AskForPet.objects.filter(user=detailPet)
    return render(request, "detailpost.html", {'detail':detailPet, 'ask': AskForPets,'photo_list':photo_list})

def delete(request, detail_id):
    if request.method == "POST":
        id = request.POST['id']
        AskForPet.objects.get(id=id).delete()
        return redirect('shannia:detail', detail_id=detail_id)
        
