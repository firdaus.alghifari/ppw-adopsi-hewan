from django.urls import path


from . import views

app_name = "shannia"

urlpatterns = [
    path('detail/<int:detail_id>/', views.detail, name='detail'),
    path('detail/<int:detail_id>/delete', views.delete),
]