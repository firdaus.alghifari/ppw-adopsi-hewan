from django.test import TestCase, Client
from django.urls import resolve
from .views import detail, delete
from .models import AskForPet
from alghi.models import Pet

class AskForPetTest(TestCase):
    def test_add_remove_comment(self):
        AskForPet.objects.create(name="Lisa", phone_number="0812384721", e_mail="lisaanhey@gmail.com", 
            message="hallo, mau tanya,apakah ame suka cakar-cakar kursi?")
        lisa = AskForPet.objects.filter(name="Lisa")
        self.assertTrue(lisa)
        lisa.delete()
        lisa = AskForPet.objects.filter(name="Lisa")
        self.assertFalse(lisa)

class DetailTest(TestCase):
    def test_detail_url_exists(self):
        Pet.objects.create(pet_name="Ame", pet_type="Cat", pet_breed="Normal", pet_gender="Male", pet_size="Large",
            pet_age="Adult", pet_health="Normal", adoption_fee="0", owner_name="Alghi", owner_phone="12345",
            owner_email="firdausalghi01@gmail.com", owner_country="Indonesia", owner_address="Pesona Khayangan",
            owner_city="Depok", owner_zipcode="12345", description="Ame is a very awesome cat that understood Depok")
        response = Client().get('/detail/1/')
        self.assertEqual(response.status_code, 200)

    def test_using_detail_view(self):
        Pet.objects.create(pet_name="Ame", pet_type="Cat", pet_breed="Normal", pet_gender="Male", pet_size="Large",
            pet_age="Adult", pet_health="Normal", adoption_fee="0", owner_name="Alghi", owner_phone="12345",
            owner_email="firdausalghi01@gmail.com", owner_country="Indonesia", owner_address="Pesona Khayangan",
            owner_city="Depok", owner_zipcode="12345", description="Ame is a very awesome cat that understood Depok")
        found = resolve('/detail/1/')
        self.assertEqual(found.func, detail)

    def test_using_detail_template(self):
        Pet.objects.create(pet_name="Ame", pet_type="Cat", pet_breed="Normal", pet_gender="Male", pet_size="Large",
            pet_age="Adult", pet_health="Normal", adoption_fee="0", owner_name="Alghi", owner_phone="12345",
            owner_email="firdausalghi01@gmail.com", owner_country="Indonesia", owner_address="Pesona Khayangan",
            owner_city="Depok", owner_zipcode="12345", description="Ame is a very awesome cat that understood Depok")
        response = Client().get('/detail/1/')
        self.assertTemplateUsed(response, 'detailpost.html')