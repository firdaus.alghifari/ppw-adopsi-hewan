from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import MessageForm
from .models import Message
from django.utils import timezone
import datetime
import os
# Create your views here.
def about(request):    
    form = MessageForm()
    if request.method =="POST":
        form=MessageForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('about')
    return render(request, "about.html", {'form':form})

def discussions(request):
    messages = Message.objects.order_by("id")
    response = {
        'messages' : messages,
        'server_time' : timezone.now(),
        }
    return render(request, "discussions.html", response)

def delete(request):
    if request.method == "POST":
        id = request.POST['id']
        Message.objects.get(id=id).delete()
        return redirect('discussions')