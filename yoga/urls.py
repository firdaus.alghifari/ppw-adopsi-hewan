from django.urls import path
from . import views

urlpatterns = [
    path('about/', views.about, name='about'),
    path('discussions/', views.discussions, name='discussions'),
    path('delete/', views.delete)
]
