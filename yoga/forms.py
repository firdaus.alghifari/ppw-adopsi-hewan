from .models import Message
from django import forms

class MessageForm(forms.ModelForm):        
        name = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "messagefields full",
                "required" : True,
                "placeholder":"Name",
                }))
        email = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "messagefields full",
                "required" : True,
                "placeholder":"example@example.com",
                }))
        subject = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "messagefields full",
                "required" : True,
                "placeholder":"Subject",
                }))
        messages = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "messagefields full",
                "required" : True,
                "placeholder":"Messages",
                }))
        class Meta:
                model = Message
                fields = ['name', 'email', 'subject', 'messages']
                
        