from django.db import models
import datetime
from django.utils import timezone
from django.core.validators import MinLengthValidator

# Create your models here.
class Message(models.Model):
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    subject = models.CharField(max_length=50)
    messages = models.CharField(max_length=100)
    def __str__(self):
        return self.subject
