from django.test import TestCase, Client
from django.urls import resolve
from .views import about, discussions, delete
from .models import Message
import datetime
from django.utils import timezone

class MessageTest(TestCase):
    def test_add_remove_messages(self):
        Message.objects.create(name="Test",email="test@test.com",subject="Subject",messages="the message")
        test = Message.objects.filter(name="Test")
        self.assertTrue(test)
        test.delete()
        test = Message.objects.filter(name="Test")
        self.assertFalse(test)

class AboutTest(TestCase):
    def test_upload_url_exists(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_using_upload_view(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_using_upload_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

class DiscussionsTest(TestCase):
    def test_discussions_url_exists(self):
        response = Client().get('/discussions/')
        self.assertEqual(response.status_code, 200)
        
    def test_using_discussions_view(self):
        found = resolve('/discussions/')
        self.assertEqual(found.func, discussions)
    
    def test_using_upload_template(self):
        response = Client().get('/discussions/')
        self.assertTemplateUsed(response, 'discussions.html')
