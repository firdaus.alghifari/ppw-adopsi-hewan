from django.shortcuts import render

from .forms import PetForm, PhotoForm
from .models import Pet, PhotoPet

def upload(request):
    if request.method == 'POST':
        pet_form = PetForm(request.POST)
        photo_pet_form = PhotoForm(request.POST, request.FILES)
        if pet_form.is_valid() and photo_pet_form.is_valid():
            new_pet = pet_form.save()
            new_photo = PhotoPet(pet = new_pet, photo = photo_pet_form.cleaned_data['photo'])
            new_photo.save()
    return render(request, 'upload.html')

def category(request):
    pet_list = Pet.objects.all()
    photo_list = PhotoPet.objects.all()
    if request.method == 'POST' and 'type' in request.POST:
        pet_list = Pet.objects.filter(pet_type = request.POST['type'])
    return render(request, 'category.html', {
        'pet_list': pet_list,
        'photo_list': photo_list
    })
