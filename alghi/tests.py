from django.test import TestCase, Client
from django.urls import resolve
from .views import upload, category
from .models import Pet

class PetTest(TestCase):
    def test_add_remove_pet(self):
        Pet.objects.create(pet_name="Ame", pet_type="Cat", pet_breed="Normal", pet_gender="Male", pet_size="Large",
            pet_age="Adult", pet_health="Normal", adoption_fee="0", owner_name="Alghi", owner_phone="12345",
            owner_email="firdausalghi01@gmail.com", owner_country="Indonesia", owner_address="Pesona Khayangan",
            owner_city="Depok", owner_zipcode="12345", description="Ame is a very awesome cat that understood Depok")
        ame = Pet.objects.filter(pet_name="Ame")
        self.assertTrue(ame)
        ame.delete()
        ame = Pet.objects.filter(pet_name="Ame")
        self.assertFalse(ame)

class UploadTest(TestCase):
    def test_upload_url_exists(self):
        response = Client().get('/upload/')
        self.assertEqual(response.status_code, 200)

    def test_using_upload_view(self):
        found = resolve('/upload/')
        self.assertEqual(found.func, upload)

    def test_using_upload_template(self):
        response = Client().get('/upload/')
        self.assertTemplateUsed(response, 'upload.html')

class CategoryTest(TestCase):
    def test_category_url_exists(self):
        response = Client().get('/category/')
        self.assertEqual(response.status_code, 200)
        
    def test_using_category_view(self):
        found = resolve('/category/')
        self.assertEqual(found.func, category)
    
    def test_using_upload_template(self):
        response = Client().get('/category/')
        self.assertTemplateUsed(response, 'category.html')
