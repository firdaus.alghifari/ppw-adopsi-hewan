from django import forms

from .models import Pet

class PetForm(forms.ModelForm):
    class Meta:
        model = Pet
        fields = [
            'pet_name','pet_type','pet_breed','pet_gender','pet_size','pet_age','pet_health',
            'adoption_fee','owner_name','owner_phone','owner_email','owner_country',
            'owner_address','owner_city','owner_zipcode','description'
        ]

class PhotoForm(forms.Form):
    photo = forms.ImageField()
