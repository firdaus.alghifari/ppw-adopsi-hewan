from django.contrib import admin

from .models import Pet, PhotoPet

admin.site.register(Pet)
admin.site.register(PhotoPet)
