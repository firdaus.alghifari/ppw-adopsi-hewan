from django.db import models

class Pet(models.Model):
    PET_TYPE_CHOICES = [
        ("Cat", "Cat"),
        ("Dog", "Dog"),
        ("Other", "Other")
    ]

    PET_GENDER = [
        ("Male", "Male"),
        ("Female", "Female")
    ]

    PET_SIZE = [
        ("Small", "Small"),
        ("Medium", "Medium"),
        ("Large", "Large"),
        ("XL", "XL")
    ]

    PET_AGE = [
        ("Young", "Young"),
        ("Adult", "Adult"),
        ("Senior", "Senior")
    ]

    pet_name = models.CharField(max_length = 50)
    pet_type = models.CharField(max_length = 50, choices = PET_TYPE_CHOICES)
    pet_breed = models.CharField(max_length = 50)
    pet_gender = models.CharField(max_length = 50, choices = PET_GENDER)
    pet_size = models.CharField(max_length = 50, choices = PET_SIZE)
    pet_age = models.CharField(max_length = 50, choices = PET_AGE)
    pet_health = models.CharField(max_length = 50, default = "")
    adoption_fee = models.CharField(max_length = 50, default = "")
    owner_name = models.CharField(max_length = 100)
    owner_phone = models.CharField(max_length = 50)
    owner_email = models.EmailField(max_length = 200)
    owner_country = models.CharField(max_length = 50)
    owner_address = models.CharField(max_length = 200)
    owner_city = models.CharField(max_length = 50)
    owner_zipcode = models.CharField(max_length = 50)
    description = models.TextField(max_length = 500)

class PhotoPet(models.Model):
    photo = models.ImageField(upload_to='images/')
    pet = models.ForeignKey(Pet, on_delete=models.CASCADE)
