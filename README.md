AdopsiPet
============

[![pipeline status](https://gitlab.com/firdaus.alghifari/ppw-adopsi-hewan/badges/master/pipeline.svg)](https://gitlab.com/firdaus.alghifari/ppw-adopsi-hewan/commits/master)
[![coverage report](https://gitlab.com/firdaus.alghifari/ppw-adopsi-hewan/badges/master/coverage.svg)](https://gitlab.com/firdaus.alghifari/ppw-adopsi-hewan/commits/master)
## Nama Kelompok :
- Muhamad Yoga Mahendra (1806205256)
- Inez Amandha Suci (1806141233)
- Firdaus Al-ghifari (1806133780)
- Shannia Dwi Melianti (1806191332)

## Link HerokuApp :
http://adopsihewan.herokuapp.com/

## Deskripsi Aplikasi dan Manfaat : 
Website Adopsi Hewan adalah platform untuk menghubungkan antara adopter 
dengan orang yang melakukan open adoption. Alasan kami mengambil ide
ini dikarenakan saat ini open adoption di Indonesia lebih sering 
dilakukan lewat media sosial, sehingga kami ingin membuat platform yang
menjadi jembatan untuk mempermudah proses adopsi. Kaitan antara platform
ini dengan Revolusi Industri 4.0 adalah membuat sistem untuk mengawasi dan 
mengakomodasi kebutuhan dari hewan peliharaan. 

Manfaat dari website ini adalah untuk mempermudah adopter dalam menemukan
hewan yang ingin diadopsi, menghubungkan adopter dengan pemilik hewan/shelter, 
mempermudah proses adopsi, serta dapat memonitor hewan peliharaan. 

Daftar fitur :
- Akun User (Register, Login, Edit Profile)
- Search Pet
- Upload Post
- Setting Post
