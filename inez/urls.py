from django.urls import path
from . import views

app_name = 'inez'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('testimonial/', views.testimonial, name='testimonial'),
]