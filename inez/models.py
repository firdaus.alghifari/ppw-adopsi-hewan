from django.db import models

# Create your models here.
class Testimonial(models.Model):
    name = models.CharField(max_length=50)
    occupation = models.CharField(max_length=50)
    testimonial = models.TextField()