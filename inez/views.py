from django.shortcuts import render
from django.shortcuts import redirect
from .forms import TestimonialForm
from .models import Testimonial

from alghi.models import Pet, PhotoPet
from alghi.forms import PetForm

# Create your views here.
def homepage(request):
    data = Testimonial.objects.all()
    pet_list = Pet.objects.all()[:3]
    photo_list = PhotoPet.objects.all()[:3]

    content = {'title' : 'Testimonials',
                'data' : data,
                'pet_list' : pet_list,
                'photo_list' : photo_list}
    return render(request, 'homepage.html', content)

def testimonial(request):
    if request.method == 'POST':
        form = TestimonialForm(request.POST)
        if form.is_valid():
                post = form.save(commit=False)
                post.save()
                return redirect('inez:homepage')
    else:
        form = TestimonialForm()

    content = {'title' : 'Post Your Testimonial',
                'form' : form}

    return render(request, 'homepage.html', content)

