from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage, testimonial
from .models import Testimonial

# Create your tests here.
class TestimonialTest(TestCase):
    def test_model_can_create_new_testimonial(self):
        #Creating a new activity
        new_testimonial = Testimonial.objects.create(name='Inez', occupation ='Student', testimonial='Terbantu sekali dengan adanya website ini')

        #Retrieving all available activity
        counting_all_available_testimonial = Testimonial.objects.all().count()
        self.assertEqual(counting_all_available_testimonial,1)

class HomepageTest(TestCase):
    def test_homepage_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_homepage_view(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_using_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')
